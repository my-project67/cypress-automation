
import{login, homepage, findproperty, propertyrequests, mytenancy} from "./Tenant-selectors"

describe("Tenant smoke test", function(){
    beforeEach(function() {
        cy.visit("/")
        cy.get(login.emailInput).type(login.emailText)
        cy.get(login.passwordInput).type(login.passwordText)
        cy.get(login.logInButton).click()
        cy.wait(5000)
        //cy.get(homepage.myJob).contains(homepage.myJobText).should('be.visible')
    });

    it("Find Properties - I should be able to search for a Property", function(){
        cy.get(findproperty.findpropertyButton).click()
        cy.get(findproperty.addressInput2).type(findproperty.addressText).wait(2000).type("{downarrow}").type("{enter}").wait(5000);
        cy.get(findproperty.submitButton).click()
        
       // cy.enterText(findproperty.addressText, '{downarrow');
      //  cy.enterText(findproperty.addressText, '{enter');
        //cy.get(properties.propertiesSiderbar).click()
        // cy.get(items.itemNameField).type(items.itemNameText, {force: true})
        // cy.get(items.categoryField).select('houseFashion', {force:true})
        // cy.get(items.subcategoryField).select('Whatsapp', {force:true})
  })  

    it("Property Requests - I should be able to view property under offer", function(){
        cy.get(propertyrequests.propertyrequestsButton).click()
        cy.get(propertyrequests.underofferButton).click()
        window.scrollTo(300, 1200)
        window.scrollTo(1200, 300)
        cy.get(propertyrequests.clickfacilities).click()
        window.scrollTo(300, 500)
        cy.wait(2000)
        cy.get(propertyrequests.propertyrequestsButton).click()
        cy.get(propertyrequests.clicktenancyapplications).click()
        window.scrollTo(300, 1200)
    })

    it("Tenancy - I should be able to view my tenancies", function(){
        cy.get(mytenancy.mytenancyButton).click({force:true})
        cy.wait(2000)
        cy.get(mytenancy.activetenanciestextpath).contains(mytenancy.activetenanciestext).should('be.visible')
        cy.get(mytenancy.pasttenancies).click()
        cy.get(mytenancy.tenancyendedtextpath).contains(mytenancy.tenancyendedtext).should('be.visible')
       
    })
})
