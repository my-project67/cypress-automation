export const login = {
emailInput: '#mat-input-0',
  emailText: 'tenant2@grr.la',
  passwordInput: '#mat-input-1',
  passwordText: 'Bamikole1@',
  logInButton: '.mat-button-wrapper > .ng-star-inserted',
  usernameText: ':nth-child(1) > .clearfix',
  googleSignBtn:'.abcRioButtonContentWrapper'
}

export const homepage = {
    myJob:'body > app-root > app-dashboard > div > cl-tenant-dashboard > div.portHealthOuter > div.portHealth_Left > div > app-myproperty > div > ul > li:nth-child(1) > h1',
    myJobText:'My Jobs'

}

export const findproperty = {
    findpropertyButton: 'body > app-root > app-dashboard-left-menu > perfect-scrollbar > div > div.ps-content > ul > li:nth-child(2) > a',
    addressInput2:'li.homeSearch',
    addresssOutput:'body > div.pac-container.pac-logo',
    addressInput: '[data-top="198"]',
    addressText: '22 Aberdeen Park London, UK',
    submitButton: '.ml10 > .mat-button-wrapper > .ng-star-inserted'
}

export const propertyrequests = {
    propertyrequestsButton: '[href="/tenant-viewing-request"]',
    underofferButton: '.letAggred',
    clickfacilities: '#mat-tab-label-0-1 > .mat-tab-label-content',
    clicktenancyapplications: '[routerlink="/tenant-tenancy-applications"] > a'
}

export const mytenancy = {
    mytenancyButton: '[href="/my-tenancies"] > .arrow',
    activetenanciestextpath: '.payTab > .ng-star-inserted',
    activetenanciestext: 'ACTIVE TENANCIES ',
    pasttenancies: '[routerlink="inactive-tenancies"]',
    tenancyendedtextpath: 'body > app-root > app-my-tenancies > div > app-inactive-tenancies > div > div:nth-child(1) > div > div.img.noImg.ng-star-inserted > span',
    tenancyendedtext: 'Tenancy Ended'

}