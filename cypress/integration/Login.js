import { login, homepage} from "./selectors";
export const validLogin = () => {
        cy.visit("/")

        cy.get(login.emailInput).type(login.emailText)
        cy.get(login.passwordInput).type(login.passwordText)
        cy.get(login.logInButton).click()
        cy.wait(3000)
        cy.get(homepage.homeTextXpath).contains(homepage.homeText).should('be.visible')
}

export const invalidLogin=()=>{
        
}
    
    