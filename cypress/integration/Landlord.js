import{ properties, sidebarMenu} from "./selectors";
const access = require("./Login");
describe("addItems", function(){
    beforeEach(function(){
        access.validLogin();
    })
    
it("Add Properties - I should be able to Add New Properties", function(){
    cy.get(properties.propertiesSiderbar).click()
    cy.get(properties.addProperty).click()
    
    cy.get(properties.clickSelectPropertyType).click()
    cy.get(properties.selectPropertyType).click()

    cy.get(properties.clickOccupancyType).click()
    cy.get(properties.selectOccupancyType).click()
    cy.get(properties.selectPropertyStatus).click()
    cy.get(properties.clickSelectPropertyStatus).click()
    cy.get(properties.selectFurnishing).click()
    cy.get(properties.clickFurnishing).click()
 
    cy.get(properties.selectFlatNo).type(properties.flatNoText, {force: true})
    cy.get(properties.selectStreetAdd).type(properties.streetAddressText, {force: true})

    cy.get(properties.selectTown).type(properties.TownText, {force: true})

    
    // cy.get(items.itemNameField).type(items.itemNameText, {force: true})
    // cy.get(items.categoryField).select('houseFashion', {force:true})
    // cy.get(items.subcategoryField).select('Whatsapp', {force:true})
  
    
})  


})
