/// <reference types="Cypress" />
import { login, homepage } from "./homeowner-selector";

describe("Device Test",function(){
    beforeEach(function () {
        cy.visit("/")
    });

  it("User Login", function (){
         //     cy.title().should('include',"Login | Kuda")
        cy.get(login.emailInput).should('be.visible').clear().type(login.emailText)
        cy.get(login.passwordInput).should('be.visible').clear().type(login.passwordText)
        cy.get(login.logInButton).should('be.visible').contains('Log in').click({ force: true })
        cy.get(homepage.homeTextXpath).should('be.visible').contains(homepage.homeText)

    })

})
