
export const locators = {
  usernameInput: '#email',
  signInPasswordInput: '#password',
  eyeIcon: '.toggle-switch > svg',
  signInButton: 'form > .btn',
  usernameText: ':nth-child(1) > .clearfix',
  passwordText: ':nth-child(2) > .clearfix',
  googleSignBtn:'.abcRioButtonContentWrapper'
}

export const login = {
  emailInput: '#mat-input-0',
  emailText: 'tfagbohungbe@gmail.com',
  passwordInput: '#mat-input-1',
  passwordText: 'Bamikole1@',
  logInButton: '.mat-button-wrapper > .ng-star-inserted',
  usernameText: ':nth-child(1) > .clearfix',
  googleSignBtn:'.abcRioButtonContentWrapper'
}

export const properties = {
  propertiesSiderbar: '.ps-content > .ng-star-inserted > :nth-child(2)',
  addProperty:'.mat-button-wrapper > .ng-star-inserted',

  clickSelectPropertyType:'#mat-select-value-9 > span',
  selectPropertyType:'#mat-option-22',

  clickOccupancyType:'#mat-select-value-11 > span',
  selectOccupancyType:'#mat-option-23 > span',

  selectPropertyStatus:'#mat-select-value-13 > span',
  clickSelectPropertyStatus:'#mat-option-21',

  selectFurnishing:'#mat-select-value-15 > span',
  clickFurnishing:'#mat-option-25',

  selectFlatNo:'#mat-input-3',
  flatNoText:'22',

  selectStreetAdd:'#autocomplete',
  streetAddressText:'22 Aberdeen Park, London, UK',
  clickStreetAdd:'#mat-select-value-9 > span',

  selectTown:'#mat-input-5',
  TownText:'AB0011',

  selectPostCode:'#mat-option-30',
  clickPostCode:'#mat-select-value-9 > span',

  selectPropertyType:'#mat-option-30',

  

  
}

export const homepage = {
  homeTextXpath: '[routerlink="/my-properties"][tabindex="0"] > h1',
  homeText: 'My Properties'
}

export const sidebarMenu = {
  teamsBtn: ':nth-child(5) > .nav-link',
  homeText: 'Home',
  itemBtn: ':nth-child(4) > .nav-link',
  settingsBtn: '.sidenav-footer > .nav > :nth-child(1) > .nav-link',
  OrderBtn: "#root > div:nth-child(1) > div > div.sidenav-main > ul > li:nth-child(2) > a"
}



export const cred = {
  // Auth component classes
  username: 'Guze',
  signInPassword: 'Obayemi92@'
}
//User Management
export const user = {
  userNgtBtn: ':nth-child(3) > :nth-child(1) > .nerve-menu--heading',

  preferredUsernameText:"Taiye"
}

export const menu={
menuMgtBtn:':nth-child(3) > :nth-child(4) > .nerve-menu--heading',
roleDescription:'Head Of Department'
}

export const items = {
addItemBtn: '.sc-fubCfw > .btn',
itemNameField: '[data-testid=name]',
itemNameText: 'Nike Shoes',
categoryField:'.mr-16 > .form-group > .form-control',
subcategoryField: '.ml-16 > .form-group > .form-control',
salesChannelField: '.gray:nth-child(1)',
salesChannel: 'li:nth-child(1) > .go3973839879 span',
itemUnitField: ':nth-child(1) > :nth-child(4) > .form-group > .form-control',
itemDescriptionField: ':nth-child(5) > .w-100p',
itemDescriptionText: 'shoes',
sellingPriceField: '[data-testid=sellingPrice]',
sellingPriceText: '10000',
costPriceField: '[data-testid=costPrice]',
costPriceText: '5000',
inventoryCheckbox: '.mr-32 > .option > .check-ticker > .check-ticker__tick',
skuField: '[data-testid=sku]',
skuText: '43524',
barcodeText: '0987890',
barcodeField: '[data-testid=barcode]',
inStockField: '.pb-16 > :nth-child(2) > .form-group > .form-control',
inStockText:'25',
lowStockText: '5',
lowStockField: '.pb-16 > :nth-child(3) > .form-group > .form-control',
saveItemBtn: '.page-modal_nav-buttons > div > .btn',
actionElipsis:':nth-child(1) > .actions > :nth-child(1) > .dropdown-toggle > svg',
editItemField: '.dropdown--open > .dropdown-item:nth-child(2)',
saveEditBtn: '.page-modal_nav-buttons > div > .btn'
}

export const teams = {
  addTeamBtn: '.actions > .btn--primary',
  firstNameField: '[data-testid=firstname]',
  firstNameText: 'dede',
  lastNameField: '[data-testid=lastname]',
  lastNameText: 'Freeze',
  emailField: '[data-testid=email]',
  emailFieldText: 'freeze@yahoo.com',
  phoneField: '[data-testid=phone]',
  phoneFieldText:'09087654567',
  categoryField: '.dropdown-heading-value > .gray',
  categorySelect:'li:nth-child(2) .go626121005',
  permissionField: ':nth-child(5) > .form-control',
  saveTeamBtn: '.page-modal_nav-buttons > :nth-child(2) > .btn'
  
}

export const settings = {
  myAccount: '[href="/app/settings/myaccount"] > .sc-iqHYGH > .settings__desc',
  changePasswrdBtn: '.mt-40 > .btn',
  currentPasswrdField: '[data-testid=oldPassword]',
  currentPasswrdText: 'password1',
  newPasswrdText: 'password',
  confirmPasswordText: 'password',
  newPasswrdField: '[data-testid=newPassword]',
  confirmPasswordField: '[data-testid=confirmPassword]',
  saveBtn: '.sc-fubCfw > .btn--primary',
  cancelBtn: '.eeNfag > .sc-gKsewC > form > .sc-fubCfw > .btn--secondary'
}

export const categories = {
  itemsCategories: '#react-tabs-2',
  addCategory: '.actions > .btn--primary',
  categoryName: '.sc-eCssSg > .sc-iBPRYJ > .form-group > [data-testid=name]',
  categoryText: 'Fashion',
  cancelCategory: '#react-tabs-3 > div.sc-gKsewC.gEuwxh.modal-backdrop > div > div.sc-pFZIQ.gbgfMs > div:nth-child(1) > button',
  saveCategory: '.sc-gKsewC > :nth-child(2) > .btn',
  actionElipsis: '.d-flx > .m-a > :nth-child(1) > .dropdown-toggle > .undefined',
  editCategory: '.dropdown--open > .dropdown-item:nth-child(2)',
  categoryName2: '#name',
  categoryText2: 'store',
  saveBtnCategory: '.sc-gKsewC > :nth-child(2) > .btn',
  cancelBtnCategory: '.eeNfag > .sc-gKsewC > .sc-fubCfw > :nth-child(1) > .btn',
  addSubcategoryBtn: '.page-modal_nav-buttons > :nth-child(2) > .btn',
  viewCategory: '.dropdown--open > .dropdown-item:nth-child(1)',
  subcategoryName: '[data-testid=name]',
  subcategoryText: 'Whatsapp',
  saveSubcategoryBtn: '.sc-gKsewC > :nth-child(2) > .btn',
  cancelSubcategoryBtn:'.eeNfag > .sc-gKsewC > .sc-fubCfw > :nth-child(1) > .btn'
}

export const signup ={
  signupBtn: '[href="/auth/signup"]',
  firstnameField: '[data-testid=firstname]',
  lastnameField:'[data-testid=lastname]',
  emailField: '[data-testid=email]',
  passwordField: '[data-testid=password]',
  continueBtn: 'form > .btn',
  firstnameText: 'Collins',
  lastnameText: 'Davies',
  emailText: 'colllin@grr.la',
  passwordText: 'Password3',
  workspaceField: '[data-testid=name]',
  workspaceText: 'Azure Kicks',
  supportEmail: '[data-testid=email]',
  supportEmailText: 'collins@grr.la',
  addressField: '[data-testid=address]',
  addressText: '13, airport road',
  phoneField: '[data-testid=phone]',
  phoneText: '09087656787',
  industryField: ':nth-child(5) > .form-control',
  doneBtn: 'form > .btn'
}
export const delAccount ={
  settingsOption: '.sidenav-footer > .nav > :nth-child(1) > .nav-link',
  workspaceSetting: 'a:nth-child(2) .grey-text',
  closeWorkspace: '.mt-40 > .btn',
  closingWorkspace: ':nth-child(1) > .check-ticker > .check-ticker__tick',
  closeWorkspaceBtn: '.sc-fubCfw > :nth-child(2) > .btn'
}
export const order ={
   viewOrder: '#root > div:nth-child(1) > main > section > div.orders > table > tbody > tr:nth-child(1) > td.orderID'
}